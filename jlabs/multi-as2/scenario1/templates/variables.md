- AREA_ID : area id - ospf v3

- MY_HOSTNAME : hostname router

- MY_LOOPBACK : loopback router xxx/128

- MY_CUSTOMER_IPv6_PREFIX : IPv6 prefix of one customer

- MY_IPv6_PREFIX: own IPv6 prefix

- MY_ASN: own AS number

- PEER_ASN: AS number of peering partner

- PEER_IPv6_PREFIX: IPv6 prefix of peering partner

- PTP_RTRX_TO_RTRY: point to point IPv6 address of router X facing router Y

- ROUTER_ID : router id

- UPSTREAM_ASN: AS number of upstream provider

- nnTAG1: right part of a community

