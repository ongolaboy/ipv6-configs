# apr/25/2021 19:51:39 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface ethernet
set [ find default-name=ether4 ] comment=to->rtr8
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=64544 name=IBGP-IPv6 redistribute-other-bgp=yes router-id=6.6.6.6
add as=64544 name=TRANSIT-AS64533-IPv6 router-id=6.6.4.4
add as=64544 name=CUSTOMER-AS65542-IPv6 redistribute-other-bgp=yes router-id=\
    6.6.8.8
/routing ospf-v3 instance
set [ find default=yes ] router-id=6.6.6.6
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:7000::1/128 advertise=no interface=loopback0
add address=2001:db8:6000:44::3/127 advertise=no comment=to-rtr4 interface=\
    ether2
add address=2001:db8:7000:700::3/127 advertise=no comment=to-rtr5 interface=\
    ether3
add address=2001:db8:7000:710::/127 advertise=no interface=ether4
/routing bgp network
add network=2001:db8:7000::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=IBGP-IPv6 name=rtr5 remote-address=\
    2001:db8:7000:700::2 remote-as=64544 update-source=2001:db8:7000::1
add address-families=ipv6 instance=TRANSIT-AS64533-IPv6 name=rtr4 \
    remote-address=2001:db8:6000:44::2 remote-as=64533
add address-families=ipv6 instance=CUSTOMER-AS65542-IPv6 name=rtr8 \
    remote-address=2001:db8:7000:710::1 remote-as=65542
/routing ospf-v3 interface
add area=backbone interface=loopback0 passive=yes
add area=backbone interface=ether3
add area=backbone interface=ether2 passive=yes
/system identity
set name=rtr6-PEER2-as64544
