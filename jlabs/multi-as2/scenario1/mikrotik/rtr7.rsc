# apr/25/2021 19:51:47 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface ethernet
set [ find default-name=ether2 ] comment=to->rtr1
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=65536 name=TRANSIT-AS64522-IPv6 router-id=7.7.1.1
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:0:1100::/128 advertise=no interface=loopback0
add address=2001:db8:5000:11c5::1/127 advertise=no interface=ether2
/routing bgp network
add network=2001:db8::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=TRANSIT-AS64522-IPv6 name=rtr1 \
    remote-address=2001:db8:5000:11c5:: remote-as=64522
/system identity
set name=rtr7-CUST1-as65536
