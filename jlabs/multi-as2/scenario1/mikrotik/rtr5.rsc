# apr/25/2021 19:51:29 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=64544 name=IBGP-IPv6 redistribute-other-bgp=yes router-id=5.5.5.5
add as=64544 name=PEER-AS64522-IPv6 router-id=5.5.3.3
/routing ospf-v3 instance
set [ find default=yes ] router-id=5.5.5.5
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:7000::/128 advertise=no interface=loopback0
add address=2001:db8:5000:11c0::3/127 advertise=no comment=to-rtr3 interface=\
    ether2
add address=2001:db8:7000:700::2/127 advertise=no comment=to-rtr6 interface=\
    ether3
/routing bgp network
add network=2001:db8:7000::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=IBGP-IPv6 name=rtr6 remote-address=\
    2001:db8:7000:700::3 remote-as=64544
add address-families=ipv6 instance=PEER-AS64522-IPv6 name=rtr3 \
    remote-address=2001:db8:5000:11c0::2 remote-as=64522
/routing ospf-v3 interface
add area=backbone interface=loopback0 passive=yes
add area=backbone interface=ether3
add area=backbone interface=ether2 passive=yes
/system identity
set name=rtr5-PEER2-as64544
