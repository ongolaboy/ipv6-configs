# apr/26/2021 20:19:40 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=64522 name=IBGP-IPv6 redistribute-other-bgp=yes router-id=3.3.3.3
add as=64522 name=PEER-AS64544-IPv6 router-id=3.3.5.5
/routing ospf-v3 instance
set [ find default=yes ] router-id=3.3.3.3
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:5000:1000::2/128 advertise=no interface=loopback0
add address=2001:db8:5000:1101::3/127 advertise=no comment=to-rtr1 interface=\
    ether2
add address=2001:db8:5000:1102::3/127 advertise=no comment=to-rtr2 interface=\
    ether3
add address=2001:db8:5000:11c0::2/127 advertise=no comment=to-rtr5 interface=\
    ether4
/routing bgp network
add network=2001:db8:5000::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=IBGP-IPv6 name=rtr1 remote-address=\
    2001:db8:5000:1101::2 remote-as=64522 update-source=2001:db8:5000:1000::2
add address-families=ipv6 instance=IBGP-IPv6 name=rtr2 remote-address=\
    2001:db8:5000:1000::1 remote-as=64522 update-source=2001:db8:5000:1000::2
add address-families=ipv6 in-filter=IPv6-IMPORT-PEER-AS64544 instance=\
    PEER-AS64544-IPv6 name=rtr5 out-filter=IPv6-EXPORT-PEER remote-address=\
    2001:db8:5000:11c0::3 remote-as=64544
/routing filter
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=::1/128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=::/128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=\
    ::ffff:0.0.0.0/96 prefix-length=96-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=100::/64 \
    prefix-length=64-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=\
    64:ff9b:1::/48 prefix-length=48-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001::/23
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001::/32
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001:2::/48 \
    prefix-length=48-128
add action=discard chain=IPv6-BOGONS disabled=yes prefix=2001:db8::/32 \
    prefix-length=32-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=fc00::/7 \
    prefix-length=7-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=fe80::/10 \
    prefix-length=10-128
add action=return chain=IPv6-BOGONS
add action=discard bgp-as-path=_0_ chain=BOGONS-ASNS comment="RFC 7607"
add action=discard bgp-as-path=_23456_ chain=BOGONS-ASNS comment=\
    "RFC 4893 AS_TRANS"
add action=discard bgp-as-path=_65535_ chain=BOGONS-ASNS comment=last16
add action=discard bgp-as-path=_4294967295_ chain=BOGONS-ASNS comment=last32
add action=return chain=BOGONS-ASNS
add action=discard bgp-communities=no-export chain=IPv6-EXPORT-PEER comment=\
    DENY
add action=accept chain=IPv6-EXPORT-PEER comment=EXPORT-ORIGINATES prefix=\
    2001:db8:5000::/36 set-bgp-communities=64522:64522
add action=accept bgp-communities=64522:9999 chain=IPv6-EXPORT-PEER comment=\
    "my customer"
add action=jump chain=IPv6-EXPORT-PEER jump-target=REJECT-ALL
add action=discard bgp-as-path-length=50-4294967295 chain=IPv6-IMPORT-PEER \
    comment="Too long path"
add action=jump chain=IPv6-IMPORT-PEER jump-target=BOGONS-ASNS
add action=discard bgp-as-path=_64522_ chain=IPv6-IMPORT-PEER comment=\
    "not my own ASN"
add action=discard bgp-as-path=_64533_ chain=IPv6-IMPORT-PEER comment=\
    "not my upstream and more generally any 'big' network"
add action=jump chain=IPv6-IMPORT-PEER jump-target=IPv6-BOGONS
add action=discard address-family=ipv6 chain=IPv6-IMPORT-PEER comment=\
    "No default" prefix=::/0
add chain=IPv6-IMPORT-PEER comment=\
    "NORMALIZE. TODO: check why it does not apply here" set-bgp-local-pref=\
    200
add action=return chain=IPv6-IMPORT-PEER
add action=jump chain=IPv6-IMPORT-PEER-AS64544 jump-target=IPv6-IMPORT-PEER
add bgp-as-path="^64544_([0-9]+)" chain=IPv6-IMPORT-PEER-AS64544 comment=\
    "only clients directly connected to my peer"
add action=accept chain=IPv6-IMPORT-PEER-AS64544 comment="AS64544 prefix" \
    prefix=2001:db8:7000::/36 prefix-length=36-48 set-bgp-local-pref=200
add action=accept chain=IPv6-IMPORT-PEER-AS64544 comment=\
    "customer. automatic method more helpful" prefix=2001:db8:4000::/36 \
    prefix-length=36-48 set-bgp-local-pref=200
add action=jump chain=IPv6-IMPORT-PEER-AS64544 jump-target=REJECT-ALL
add action=discard chain=REJECT-ALL
/routing ospf-v3 interface
add area=backbone interface=ether2
add area=backbone interface=ether3
add area=backbone interface=loopback0 passive=yes
add area=backbone interface=ether4 passive=yes
/system identity
set name=rtr3-PEER1-as64522
