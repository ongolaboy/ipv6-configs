# apr/25/2021 19:51:22 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=64533 name=CUSTOMER-AS64522-IPv6 redistribute-other-bgp=yes router-id=\
    4.4.2.2
add as=64533 name=CUSTOMER-AS64544-IPv6 redistribute-other-bgp=yes router-id=\
    4.4.6.6
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:6000:100::/128 advertise=no interface=loopback0
add address=2001:db8:6000:40::2/127 advertise=no comment=to-rtr2 interface=\
    ether2
add address=2001:db8:6000:44::2/127 advertise=no comment=to-rtr6 interface=\
    ether3
/routing bgp network
add network=2001:db8:6000::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=CUSTOMER-AS64522-IPv6 name=rtr2 \
    remote-address=2001:db8:6000:40::3 remote-as=64522
add address-families=ipv6 instance=CUSTOMER-AS64544-IPv6 name=rtr6 \
    remote-address=2001:db8:6000:44::3 remote-as=64544
/system identity
set name=rtr4-ISP-as64533
