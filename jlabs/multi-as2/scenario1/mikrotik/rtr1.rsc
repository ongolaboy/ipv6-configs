# apr/26/2021 20:19:28 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface ethernet
set [ find default-name=ether4 ] comment=to->rtr7
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=64522 name=IBGP-IPv6 redistribute-other-bgp=yes router-id=1.1.1.1
add as=64522 name=CUSTOMER-AS65536-IPv6 redistribute-other-bgp=yes router-id=\
    1.1.7.7
/routing ospf-v3 instance
set [ find default=yes ] router-id=1.1.1.1
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:5000:1000::/128 advertise=no interface=loopback0
add address=2001:db8:5000:1101::2/127 advertise=no comment=to-rtr3 interface=\
    ether3
add address=2001:db8:5000:1100::/127 advertise=no comment=to-rtr2 interface=\
    ether2
add address=2001:db8:5000:11c5::/127 advertise=no interface=ether4
/routing bgp peer
add address-families=ipv6 in-filter=IPv6-IMPORT-CUSTOMER-AS65536 instance=\
    CUSTOMER-AS65536-IPv6 name=rtr7 out-filter=IPv6-EXPORT-CUSTOMER \
    remote-address=2001:db8:5000:11c5::1 remote-as=65536
add address-families=ipv6 instance=IBGP-IPv6 name=rtr2 nexthop-choice=\
    force-self remote-address=2001:db8:5000:1100::1 remote-as=64522
add address-families=ipv6 instance=IBGP-IPv6 name=rtr3 nexthop-choice=\
    force-self remote-address=2001:db8:5000:1101::3 remote-as=64522
/routing filter
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=::1/128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=::/128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=\
    ::ffff:0.0.0.0/96 prefix-length=96-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=100::/64 \
    prefix-length=64-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=\
    64:ff9b:1::/48 prefix-length=48-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001::/23
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001::/32
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=2001:2::/48 \
    prefix-length=48-128
add action=discard chain=IPv6-BOGONS disabled=yes prefix=2001:db8::/32 \
    prefix-length=32-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=fc00::/7 \
    prefix-length=7-128
add action=discard address-family=ipv6 chain=IPv6-BOGONS prefix=fe80::/10 \
    prefix-length=10-128
add action=return chain=IPv6-BOGONS
add action=discard bgp-as-path=_0_ chain=BOGONS-ASNS comment="RFC 7607"
add action=discard bgp-as-path=_23456_ chain=BOGONS-ASNS comment=\
    "RFC 4893 AS_TRANS"
add action=discard bgp-as-path=_65535_ chain=BOGONS-ASNS comment=last16
add action=discard bgp-as-path=_4294967295_ chain=BOGONS-ASNS comment=last32
add action=return chain=BOGONS-ASNS
add action=accept chain=IPv6-IMPORT-CUSTOMER-AS65536 comment=NORMALIZE \
    prefix=2001:db8::/36 set-bgp-communities=64522:9999 set-bgp-local-pref=\
    500
add action=accept chain=IPv6-IMPORT-CUSTOMER-AS65536 comment=\
    "accept more specific from this customer" prefix=2001:db8::/36 \
    prefix-length=36-48 set-bgp-communities=64522:9999
add action=jump chain=IPv6-IMPORT-CUSTOMER-AS65536 jump-target=REJECT-ALL
add action=discard bgp-communities=no-export chain=IPv6-EXPORT-CUSTOMER \
    comment=DENY
add action=accept bgp-communities=64522:64522 chain=IPv6-EXPORT-CUSTOMER \
    comment=EXPORT-ORIGINATES
add action=accept chain=IPv6-EXPORT-CUSTOMER protocol=bgp
add action=jump chain=IPv6-EXPORT-CUSTOMER jump-target=REJECT-ALL
add action=discard chain=REJECT-ALL
/routing ospf-v3 interface
add area=backbone interface=ether3
add area=backbone interface=ether2
add area=backbone interface=loopback0 passive=yes
add area=backbone interface=ether4 passive=yes
/system identity
set name=rtr1-PEER1-as64522
