# apr/25/2021 19:51:52 by RouterOS 6.48.2
# software id = 
#
#
#
/interface bridge
add name=loopback0
/interface ethernet
set [ find default-name=ether2 ] comment=to->rtr6
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/routing bgp instance
set default disabled=yes
add as=65542 name=TRANSIT-AS64544-IPv6 router-id=8.8.6.6
/ip dhcp-client
add disabled=no interface=ether1
/ipv6 address
add address=2001:db8:4000:: interface=loopback0
add address=2001:db8:7000:710::1/127 advertise=no interface=ether2
/routing bgp network
add network=2001:db8:4000::/36 synchronize=no
/routing bgp peer
add address-families=ipv6 instance=TRANSIT-AS64544-IPv6 name=rtr6 \
    remote-address=2001:db8:7000:710:: remote-as=64544
/system identity
set name=rtr8-CUST2-as65542
