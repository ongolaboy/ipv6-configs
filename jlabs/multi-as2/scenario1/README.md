We will simulate interconnections between several ASN having different purposes.

There are configuration file for each platform

- cisco
- juniper
- mikrotik

There is a folder 'template' for each platform.


# Routing policy

Let's consider 3 kind of network types
- Peer : exchange traffic with another network
- Transit: route traffic through/from other networks for a customer
- Customer: send/receive traffic to/from a transit provider

We will design our rules depending on :
- what do we announce?
- what routes shall we accept from other network types?



## Announcing routes

### To customers

- A default route
- A full table
- Or a combination of a full table and a default route

### To peers

- your own routes
- your customers' routes

### To transit provider

- your own routes
- your customers' routes


## Receiving routes

### From customers

- Only accept prefixes that have a valid route object in an IRR database
- Only accept autonomous system numbers downstream of your customer that have a valid registration in your customers’ AS-SET
- Only accept prefix lengths up to /48 IPv6
- Only accept RPKI valid and unknown routes. Reject RPKI invalid.
- Reject routes containing a bogon autonomous system number, bogon prefix, prefix longer than /48, or AS path of unreasonable length.
- Reject routes you originate yourself.

### From peers

- Received routes from a peer should never contain the AS number of a known transit provider
- Reject routes containing a bogon autonomous system number, bogon prefix, prefix longer than /48, or AS path of unreasonable length.
- Accept only peers' prefix and its customers' prefixes.

### From transit provider

- reject RPKI invalid
- Reject routes containing a bogon autonomous system number, bogon prefix, prefix longer than /48, or AS path of unreasonable length.
- Reject routes you originate yourself.
- Accept the rest.



# Infrastructure


## Address planning


### Design decision

- each loopback will get 1x/128
- each ptp link will be assigned 1x/127 from 1x/64

### Allocation

- AS64522: 2001:db8:5000::/36
- AS64533: 2001:db8:6000::/36
- AS64544: 2001:db8:7000::/36
- AS65536: 2001:db8::/36
- AS65537: 2001:db8:1000::/36
- AS65538: 2001:db8:f000::/36
- AS65539: 2001:db8:e000::/36
- AS65540: 2001:db8:2000::/36
- AS65541: 2001:db8:3000::/36
- AS65542: 2001:db8:4000::/36


# Relationship between AS

- Peers: AS64522 & AS64544
- Customers of AS64522: AS65536
- Customers of AS64544: AS65542
- Transit: AS64533

# Network diagram

Our network diagram is based on the one you can find on juniper vlabs [here](https://jlabs.juniper.net/assets/img/campaign/vlabs/bgp-ospf-diagram.jpg)

## AS

```mermaid
graph TD;
  AS64522-->AS64533;
  AS64544-->AS64533;
  AS64522-->AS64544;
```

## Routers

```mermaid
graph TD;
  vmx1-->vmx2;
  vmx1-->vmx3;
  vmx2-->vmx3;
  vmx2-->vmx4;
  vmx3-->vmx5;
  vmx4-->vmx6;
  vmx5-->vmx6;
```

## Remarks

- No IPv4-only configuration applied
- We are using the IPv6 prefix documentation 2001:db8::/32 and sub-prefixes. Thus, we are not filtering it here. In real world, you will filter out :)
- we set a local preference of 100 to routes learned from upstream
- we set a local preference of 200 to routes learned from peers
- we use the community [MYASN]:[MYASN] to designate routes we originate
- we use the community [MYASN]:9999 to designate routes we learn from peers or customers
- We do not include yet RPKI validation


# References

## Juniper
We built our configuration based upon:

- [Network diagram](https://jlabs.juniper.net/assets/img/campaign/vlabs/bgp-ospf-diagram.jpg)
- [BGP Filter Guide](http://bgpfilterguide.nlnog.net/)
- "DAY ONE: DEPLOYING BGP ROUTING SECURITY" By Melchior Aelmans & Niels Raijer

## Mikrotik

- https://mum.mikrotik.com/presentations/HR13/maia.pdf
